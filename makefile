SOURCE = main.c
TARGET = onevm
CC = gcc
CC_FLAGS = -std=c11

all:
	${CC} ${CC_FLAGS} ${SOURCE} -o ${TARGET}

clean:
	rm -rf *.o